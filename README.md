				Replenishment Supplier Master

System Requirements:
 - Visual Studio 2017
 - SQL Server Management Studio
 - .net framework 4.7
 - IIS > 7

Database Install:

 - Create Database ‘RSM’ and Tables.
 - ReplenishmentSupplierMasterTable (Create triggers)
 - [Replenishment_Trigger_Insert]
 - [Replenishment_Trigger_Update]
 - [Replenishment_Trigger_Delete]
 - LoggedData
 - Logs

Application Install:

 - Download the source code then extract it. 
 - Click on the ‘sln’ file. It will load project in visual studio with required packages. 
 - To load node modules, right click on package.json and click on restore packages. 
 - Now build the project.
 - Navigate through ‘app’ folder to see where the angular component is bootstrapped. 

Frameworks:

 - Angular 5 
 - ASP.net MVC 6 Entity framework.
 

Note :

 - You can find the SQL script files along with project code.