USE [RSM]
GO

/****** Object:  Table [dbo].[Log]    Script Date: 2/7/2018 1:10:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Log](
	[id] [int] NULL,
	[OldCOST] [nvarchar](255) NULL,
	[OldPROMO $] [nvarchar](255) NULL,
	[NewCOST] [nvarchar](255) NULL,
	[NewPROMO $] [nvarchar](255) NULL
) ON [PRIMARY]

GO


