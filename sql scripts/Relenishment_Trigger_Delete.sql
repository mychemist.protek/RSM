USE [RSM]
GO

/****** Object:  Trigger [dbo].[Replenishment_Trigger_Delete]    Script Date: 2/7/2018 1:08:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE trigger [dbo].[Replenishment_Trigger_Delete] on [dbo].[ReplenishmentSupplierMasterTable]
after delete
as
 
select d.MychemId id, d.COST OldCOST,d.[PROMO $] [OldPROMO $] into #Log
from deleted d 
BEGIN
insert into LoggedData(MyChemId,Beforechanges,Afterchanges,Action,Date) select id, 'cost ='+ISNULL(OldCOST,'null')+', promo ='+ISNULL([OldPROMO $],'null'),
'N/A','Delete',getdate() from #Log
END



GO


