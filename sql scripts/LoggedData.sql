USE [RSM]
GO

/****** Object:  Table [dbo].[LoggedData]    Script Date: 2/7/2018 1:10:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LoggedData](
	[MyChemId] [int] NULL,
	[Beforechanges] [varchar](50) NULL,
	[Afterchanges] [varchar](50) NULL,
	[Action] [varchar](20) NULL,
	[Date] [datetime] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_LoggedData] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


