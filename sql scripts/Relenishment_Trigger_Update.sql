USE [RSM]
GO

/****** Object:  Trigger [dbo].[Replenishment_Trigger_Update]    Script Date: 2/7/2018 1:08:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE trigger [dbo].[Replenishment_Trigger_Update] on [dbo].[ReplenishmentSupplierMasterTable]
after Update
as
 
 
select d.MychemId id, d.COST OldCOST,d.[PROMO $] [OldPROMO $],i.COST NewCOST,i.[PROMO $] [NewPROMO $]
into #Log
from deleted d join Inserted i on i.MychemId=d.MychemId

BEGIN
insert into LoggedData(MyChemId,Beforechanges,Afterchanges,Action,Date) 
select id, 'cost ='+ISNUll(OldCOST,'null')+', promo ='+ISNULL([OldPROMO $],'null'),
'cost ='+ISNUll( NewCOST,'null')+', promo ='+ISnull([NewPROMO $],'null'),'Update',getdate() from #Log

END

GO


