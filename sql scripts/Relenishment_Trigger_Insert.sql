USE [RSM]
GO

/****** Object:  Trigger [dbo].[Replenishment_Trigger_Insert]    Script Date: 2/7/2018 1:07:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE trigger [dbo].[Replenishment_Trigger_Insert] on [dbo].[ReplenishmentSupplierMasterTable]
after Insert
as
select i.MychemId id,i.COST NewCOST,i.[PROMO $] [NewPROMO $] into #Log
from  Inserted i 
BEGIN
insert into LoggedData(MyChemId,Beforechanges,Afterchanges,Action,Date) select id, 'N/A',
'cost ='+ISNULL(NewCOST,'null')+', promo ='+ISNULL([NewPROMO $],'null'),'Insert',getdate() from #Log
END

GO


