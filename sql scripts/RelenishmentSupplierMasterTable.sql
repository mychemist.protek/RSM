USE [RSM]
GO

/****** Object:  Table [dbo].[ReplenishmentSupplierMasterTable]    Script Date: 2/7/2018 1:09:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ReplenishmentSupplierMasterTable](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SUPPLIER] [nvarchar](255) NULL,
	[CODE] [nvarchar](255) NULL,
	[VCODE] [nvarchar](255) NULL,
	[PLU] [float] NULL,
	[MychemId] [int] NOT NULL,
	[DESCRIPTION ] [nvarchar](255) NULL,
	[COST] [nvarchar](255) NULL,
	[PROMO $] [nvarchar](255) NULL,
	[MOQ] [float] NULL,
	[CONTACT DETAILS] [nvarchar](255) NULL,
	[NOTES] [nvarchar](255) NULL,
 CONSTRAINT [PK__Replenis__18F6E290BD09420A] PRIMARY KEY CLUSTERED 
(
	[MychemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


