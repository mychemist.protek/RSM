"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var animations_1 = require("@angular/platform-browser/animations");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var ng2_toastr_1 = require("ng2-toastr/ng2-toastr");
var app_component_1 = require("./app.component");
var custom_grid_component_1 = require("./custom-grid/custom.grid.component");
var custom_grid_sort_pipe_1 = require("./pipes/custom.grid.sort.pipe");
var pagination_service_1 = require("./custom-grid/pagination.service");
var excel_service_1 = require("./services/excel.service");
var confirm_directive_1 = require("./confirm.directive");
var content_editable_directive_1 = require("./directives/content.editable.directive");
//import { ProgressHttpModule } from 'angular-progress-http';
var Tabset_1 = require("./tab-component/Tabset");
var sortgrid_pipe_1 = require("./pipes/sortgrid.pipe");
var ngx_progressbar_1 = require("ngx-progressbar");
var rsmApi_service_1 = require("./services/rsmApi.service");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, http_1.HttpModule, forms_1.FormsModule, animations_1.BrowserAnimationsModule, ng2_toastr_1.ToastModule.forRoot(), ngx_progressbar_1.NgProgressModule],
            declarations: [app_component_1.AppComponent,
                custom_grid_component_1.CustomGridComponent, custom_grid_sort_pipe_1.CustomGridSortPipe, confirm_directive_1.ConfirmDirective, content_editable_directive_1.EditableDivDirective, Tabset_1.TAB_COMPONENTS, sortgrid_pipe_1.SortGridPipe],
            providers: [custom_grid_sort_pipe_1.CustomGridSortPipe, pagination_service_1.PagingService, excel_service_1.ExcelService, rsmApi_service_1.RsmApiService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
