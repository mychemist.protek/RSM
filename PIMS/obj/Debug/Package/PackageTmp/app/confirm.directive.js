"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("/node_modules/jquery-confirm/js/jquery-confirm.js");
var ConfirmDirective = /** @class */ (function () {
    function ConfirmDirective() {
        this.click = new core_1.EventEmitter();
    }
    ConfirmDirective.prototype.clicked = function (e) {
        var _this = this;
        jQuery.confirm({
            animationSpeed: 100,
            title: "",
            content: "Are you sure you want to delete MyChemId:" + this.confirm + " ? ",
            buttons: {
                confirm: {
                    btnClass: 'confirm-alert-btn',
                    action: function () { return _this.click.emit(); }
                },
                cancel: {
                    btnClass: 'cancel-alert-btn',
                    action: function () { }
                }
            }
        });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ConfirmDirective.prototype, "confirm", void 0);
    __decorate([
        core_1.Output('confirm-click'),
        __metadata("design:type", Object)
    ], ConfirmDirective.prototype, "click", void 0);
    __decorate([
        core_1.HostListener('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], ConfirmDirective.prototype, "clicked", null);
    ConfirmDirective = __decorate([
        core_1.Directive({
            selector: '[confirm]'
        })
    ], ConfirmDirective);
    return ConfirmDirective;
}());
exports.ConfirmDirective = ConfirmDirective;
