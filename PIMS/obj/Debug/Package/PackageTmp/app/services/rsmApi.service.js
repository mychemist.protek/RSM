"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/throw");
var RsmApiService = /** @class */ (function () {
    function RsmApiService(http) {
        this.http = http;
    }
    //Get all rows
    RsmApiService.prototype.getRsmData = function () {
        return this.http.get('api/RsmApi').map(function (res) { return res.json(); })
            .catch(function (error) { return Observable_1.Observable.throw(error); });
    };
    // Add a row
    RsmApiService.prototype.addNewRow = function (body) {
        return this.http.post('api/RsmApi', body)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return Observable_1.Observable.throw(error); });
    };
    // Update a row
    RsmApiService.prototype.updateRow = function (body) {
        return this.http.put('api/RsmApi/' + body['id'], body['item'])
            .map(function (res) { return res.json(); })
            .catch(function (error) { return Observable_1.Observable.throw(error); });
    };
    // Delete a row
    RsmApiService.prototype.deleteRow = function (id) {
        return this.http.delete('api/RsmApi/' + id)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return Observable_1.Observable.throw(error); });
    };
    // Upload a sheet
    RsmApiService.prototype.uploadSheet = function (body) {
        return this.http.post('api/saveExcelData', body)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return Observable_1.Observable.throw(error); });
    };
    //Get activity log data
    RsmApiService.prototype.getLogData = function () {
        return this.http.get('api/LoggedData').map(function (res) { return res.json(); })
            .catch(function (error) { return Observable_1.Observable.throw(error); });
    };
    RsmApiService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], RsmApiService);
    return RsmApiService;
}());
exports.RsmApiService = RsmApiService;
