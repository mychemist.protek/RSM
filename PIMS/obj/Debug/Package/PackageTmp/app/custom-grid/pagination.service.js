"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var PagingService = /** @class */ (function () {
    function PagingService() {
    }
    PagingService.prototype.paginationProperties = function (totalItems, currentPage, pageSize, totalPageLinkButtons) {
        if (currentPage === void 0) { currentPage = 1; }
        if (totalPageLinkButtons === void 0) { totalPageLinkButtons = 5; }
        pageSize = Number(pageSize);
        totalItems = totalItems || 1;
        currentPage = currentPage || 1;
        pageSize = pageSize || 10;
        totalPageLinkButtons = totalPageLinkButtons || 10;
        var totalPages = Math.ceil(totalItems / pageSize);
        var firstPage;
        var lastPage;
        if (totalPages <= totalPageLinkButtons) {
            firstPage = 1;
            lastPage = totalPages;
        }
        else {
            if (currentPage <= Math.ceil(totalPageLinkButtons / 2)) {
                firstPage = 1;
                lastPage = totalPageLinkButtons;
            }
            else if (currentPage + Math.ceil(totalPageLinkButtons / 2) > totalPages) {
                firstPage = totalPages - totalPageLinkButtons + 1;
                lastPage = totalPages;
            }
            else {
                firstPage = currentPage - Math.ceil(totalPageLinkButtons / 2) + 1;
                lastPage = firstPage + totalPageLinkButtons - 1;
            }
        }
        var firstIndex = (currentPage - 1) * pageSize;
        var lastIndex = Math.min(firstIndex + pageSize - 1, totalItems - 1);
        var pages = [];
        for (var i = firstPage; i <= lastPage; i++) {
            pages.push(i);
        }
        return {
            firstPage: firstPage,
            lastPage: lastPage,
            firstIndex: firstIndex,
            lastIndex: lastIndex,
            totalPageLinkButtons: totalPageLinkButtons,
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            pages: pages
        };
    };
    PagingService = __decorate([
        core_1.Injectable()
    ], PagingService);
    return PagingService;
}());
exports.PagingService = PagingService;
