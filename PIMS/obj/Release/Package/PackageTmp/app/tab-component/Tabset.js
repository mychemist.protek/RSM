"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Tab_1 = require("./Tab");
var Tabset = /** @class */ (function () {
    function Tabset() {
        this.onSelect = new core_1.EventEmitter();
    }
    Tabset.prototype.ngAfterContentInit = function () {
        var tabs = this.tabs.toArray();
        var actives = this.tabs.filter(function (t) { return t.active; });
        if (actives.length > 1) {
            console.error("Multiple active tabs set 'active'");
        }
        else if (!actives.length && tabs.length) {
            tabs[0].active = true;
        }
    };
    Tabset.prototype.tabClicked = function (tab) {
        var tabs = this.tabs.toArray();
        tabs.forEach(function (tab) { return tab.active = false; });
        tab.active = true;
        this.onSelect.emit(tab);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], Tabset.prototype, "vertical", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], Tabset.prototype, "onSelect", void 0);
    __decorate([
        core_1.ContentChildren(Tab_1.Tab),
        __metadata("design:type", Object)
    ], Tabset.prototype, "tabs", void 0);
    Tabset = __decorate([
        core_1.Component({
            selector: 'tabset',
            template: "\n    <section class=\"tab-set\">\n      <ul\n        class=\"nav\"\n        [class.nav-pills]=\"vertical\"\n        [class.nav-tabs]=\"!vertical\">\n        <li\n          *ngFor=\"let tab of tabs\"\n          [class.active]=\"tab.active\">\n          <a\n            (click)=\"tabClicked(tab)\"\n            class=\"btn\"\n            [class.disabled]=\"tab.disabled\">\n            <span>{{tab.title}}</span>\n          </a>\n        </li>\n      </ul>\n      <div class=\"tab-content\">\n        <ng-content></ng-content>\n      </div>\n    </section>\n  "
        })
    ], Tabset);
    return Tabset;
}());
exports.Tabset = Tabset;
exports.TAB_COMPONENTS = [
    Tabset,
    Tab_1.Tab
];
