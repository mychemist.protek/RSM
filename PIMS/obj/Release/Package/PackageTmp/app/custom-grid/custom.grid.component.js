"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var pagination_service_1 = require("./pagination.service");
var CustomGridComponent = /** @class */ (function () {
    function CustomGridComponent(http, pagingService) {
        this.http = http;
        this.pagingService = pagingService;
        this.updateRow = new core_1.EventEmitter();
        this.deleteRow = new core_1.EventEmitter();
        this.addNewRow = new core_1.EventEmitter();
        this.uploadSheet = new core_1.EventEmitter();
        this.exportToExcel = new core_1.EventEmitter();
        this.sortDirection = false;
        this.selectPageSize = 500;
        this.searchFields = {};
        this.pager = {};
        this.newRow = {};
    }
    CustomGridComponent.prototype.ngOnInit = function () {
        this.sortColumn = this.gridOptions.columnDefs[0].column;
        this.gridData = this.gridOptions.data;
        this.setPage(1);
    };
    CustomGridComponent.prototype.updateGrid = function () {
        this.gridData = this.gridOptions.data;
        this.setPage(1);
    };
    CustomGridComponent.prototype.onKeyPressedSearch = function (e) {
    };
    CustomGridComponent.prototype.onAddNewRow = function () {
        this.searchQuery = '';
        this.shouldAddNewRow = true;
    };
    CustomGridComponent.prototype.onSaveNewRow = function () {
        //this.pagedItems.push(this.newRow);
        if (this.newRow.MychemId) {
            this.addNewRow.next(this.newRow);
        }
        else {
            alert('Please enter MyChemId');
        }
    };
    CustomGridComponent.prototype.addOnEnter = function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            this.onSaveNewRow();
        }
    };
    CustomGridComponent.prototype.onUploadSheet = function (event) {
        this.searchQuery = '';
        this.uploadSheet.next(event);
    };
    CustomGridComponent.prototype.onExportToExcel = function (event) {
        this.exportToExcel.next(this.gridData);
    };
    CustomGridComponent.prototype.onResetAddNewForm = function () {
        this.newRow = {};
        this.shouldAddNewRow = false;
    };
    CustomGridComponent.prototype.onCancelAddRow = function () {
        this.onResetAddNewForm();
    };
    CustomGridComponent.prototype.onSearchData = function () {
        var _this = this;
        if (this.searchQuery) {
            this.gridData = this.gridOptions.data.filter(function (itemRow) {
                var referenceStr = '';
                for (var key in itemRow) {
                    referenceStr += itemRow[key];
                }
                return referenceStr.toLowerCase().indexOf(_this.searchQuery.toLowerCase()) > -1;
            });
        }
        else {
            this.gridData = this.gridOptions.data;
        }
        this.setPage(1);
    };
    CustomGridComponent.prototype.searchOnData = function (event) {
        if (event.keyCode === 13) {
            this.onSearchData();
        }
    };
    CustomGridComponent.prototype.editRow = function (row) {
        var _this = this;
        this.pagedItems.forEach(function (item, i) {
            if (row[_this.gridOptions.rowId] === item[_this.gridOptions.rowId]) {
                item.editDetails = __assign({}, item);
                item.edit = true;
            }
        });
    };
    CustomGridComponent.prototype.goToPage = function () {
        this.setPage(this.pager.currentPage);
    };
    CustomGridComponent.prototype.onSelectPageSize = function () {
        this.setPage(1);
    };
    CustomGridComponent.prototype.onUpdateRow = function (row) {
        var _this = this;
        this.pagedItems.forEach(function (item, i) {
            if (row[_this.gridOptions.rowId] === item[_this.gridOptions.rowId]) {
                var editDetails = item['editDetails'];
                _this.pagedItems[i] = editDetails;
                var obj = {
                    id: row[_this.gridOptions.rowId],
                    item: editDetails
                };
                item.edit = false;
                _this.updateRow.next(obj);
            }
        });
    };
    CustomGridComponent.prototype.cancel = function (row) {
        var _this = this;
        this.pagedItems.forEach(function (item, i) {
            if (row[_this.gridOptions.rowId] === item[_this.gridOptions.rowId]) {
                item.edit = false;
            }
        });
    };
    CustomGridComponent.prototype.onDeletRow = function (row) {
        var _this = this;
        this.pagedItems.forEach(function (item, i) {
            if (row[_this.gridOptions.rowId] === item[_this.gridOptions.rowId]) {
                _this.pagedItems.splice(i, 1);
                _this.deleteRow.next(row[_this.gridOptions.rowId]);
            }
        });
    };
    CustomGridComponent.prototype.onClearSearch = function () {
        this.searchQuery = '';
        this.onSearchData();
    };
    CustomGridComponent.prototype.onGridSort = function (row) {
        this.sortColumn = row.column;
        this.sortDirection = !this.sortDirection;
    };
    CustomGridComponent.prototype.setPage = function (page) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        this.pager = this.pagingService.paginationProperties(this.gridData.length, page, this.selectPageSize);
        this.pagedItems = this.gridData.slice(this.pager.firstIndex, this.pager.lastIndex + 1);
    };
    CustomGridComponent.prototype.saveOnEnter = function (event, row, chemId) {
        if (event.keyCode === 13) {
            if (chemId) {
                this.onUpdateRow(row);
            }
            event.preventDefault();
        }
        else if (event.keyCode === 27) {
            this.cancel(row);
        }
    };
    CustomGridComponent.prototype.handleKeyboardEvent = function (event) {
        if (event.keyCode === 27) {
            this.onResetAddNewForm();
            if (this.searchQuery) {
                this.searchQuery = '';
                this.onSearchData();
            }
            this.pagedItems.forEach(function (item, i) {
                item.edit = false;
            });
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], CustomGridComponent.prototype, "gridOptions", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], CustomGridComponent.prototype, "updateRow", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], CustomGridComponent.prototype, "deleteRow", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], CustomGridComponent.prototype, "addNewRow", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], CustomGridComponent.prototype, "uploadSheet", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], CustomGridComponent.prototype, "exportToExcel", void 0);
    __decorate([
        core_1.HostListener('document:keydown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], CustomGridComponent.prototype, "handleKeyboardEvent", null);
    CustomGridComponent = __decorate([
        core_1.Component({
            selector: 'custom-grid',
            templateUrl: './custom.grid.component.html',
            styleUrls: ['./custom.grid.component.css']
        }),
        __metadata("design:paramtypes", [http_1.Http, pagination_service_1.PagingService])
    ], CustomGridComponent);
    return CustomGridComponent;
}());
exports.CustomGridComponent = CustomGridComponent;
