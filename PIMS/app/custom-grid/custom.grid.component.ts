import { Component, OnInit, Input, EventEmitter, Output, HostListener, ElementRef, ViewChild } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { IGridOptions, IColumnDefs, IRsmTable, IPagination } from '../Igrid.interface';
import { PagingService } from './pagination.service';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'custom-grid',
    templateUrl: './custom.grid.component.html',
    styleUrls: ['./custom.grid.component.css']
  
})

export class CustomGridComponent implements OnInit {
    @Input() gridOptions: IGridOptions; 
    @Output() updateRow = new EventEmitter();
    @Output() deleteRow = new EventEmitter();
    @Output() addNewRow = new EventEmitter();
    @Output() uploadSheet = new EventEmitter();
    @Output() exportToExcel = new EventEmitter();
    sortColumn: string;
    shouldAddNewRow: boolean;
    sortDirection = false;
    selectPageSize: number = 500;
    searchQuery: string;
    searchFields: IRsmTable = {};
    pager: IPagination = {};
    pagedItems: Array<IRsmTable>;
    newRow: IRsmTable = {};
   // http: any;    
    gridData: Array<IRsmTable>;
    @ViewChild('scrollMe') private myScrollContainer: ElementRef;
    constructor(private http: Http, private pagingService: PagingService) {       
    }
    ngOnInit() {
        this.sortColumn = this.gridOptions.columnDefs[0].column;
        this.gridData = this.gridOptions.data;
        this.setPage(1);
             
    }

    updateGrid() {
        this.gridData = this.gridOptions.data;
        this.setPage(1);
    }

    onKeyPressedSearch(e: Event) {

    }

    onAddNewRow() {
        this.myScrollContainer.nativeElement.scrollTop = 0;
        this.shouldAddNewRow = true;
        if (this.searchQuery) {
            this.searchQuery = '';
            this.gridData = this.gridOptions.data;
            this.setPage(1);
        }
        
       
    }

    onSaveNewRow() {

        //this.pagedItems.push(this.newRow);
        if (this.newRow.MychemId) {
            this.addNewRow.next(this.newRow);
        } else {
            alert('Please enter MyChemId');
        }
             
    }

    addOnEnter(event:any) {
        if (event.keyCode === 13) {
            event.preventDefault();
            this.onSaveNewRow();
        }
    }

    onUploadSheet(event: any) {
        this.searchQuery = '';
        this.uploadSheet.next(event);
    }

    onExportToExcel(event: any) {
        this.exportToExcel.next(this.gridData);
    }

    onResetAddNewForm() {
        this.newRow = {};
        this.shouldAddNewRow = false;
    }

    onCancelAddRow() {
        this.onResetAddNewForm();
    }

    onSearchData() {
        this.shouldAddNewRow = false;
        if (this.searchQuery) {
            this.gridData = this.gridOptions.data.filter((itemRow) => {
                let referenceStr = '';
                for (const key in itemRow) {
                    referenceStr += itemRow[key];
                }
                return referenceStr.toLowerCase().indexOf(this.searchQuery.toLowerCase()) > -1;
            });
        } else {
            this.gridData = this.gridOptions.data;
        }
        this.setPage(1);
    }

    searchOnData(event: any) {
        if (event.keyCode === 13) {
            this.onSearchData()
        }
    }
 

    editRow(row: IRsmTable): void {
        this.pagedItems.forEach((item, i) => {
            if (row[this.gridOptions.rowId] === item[this.gridOptions.rowId]) {
                item.editDetails = { ...item };
                item.edit = true;   
            } 
        });
    }

    goToPage() {
        this.setPage(this.pager.currentPage);
    }

    onSelectPageSize() {
        this.setPage(1);
    }

    onUpdateRow(row: IRsmTable): any {
        this.pagedItems.forEach((item, i) => {
            if (row[this.gridOptions.rowId] === item[this.gridOptions.rowId]) {
                let editDetails = item['editDetails']
                this.pagedItems[i] = editDetails;
                let obj = {
                    id: row[this.gridOptions.rowId],
                    item: editDetails
                }
                item.edit = false;
                this.updateRow.next(obj);
            }
        });
    }

    cancel(row: IRsmTable): void {
        this.pagedItems.forEach((item, i) => {
            if (row[this.gridOptions.rowId] === item[this.gridOptions.rowId]) {              
                item.edit = false;
            }
        });
    }

    onDeletRow(row: IRsmTable): void {
        this.pagedItems.forEach((item, i) => {
            if (row[this.gridOptions.rowId] === item[this.gridOptions.rowId]) {
                this.pagedItems.splice(i, 1);
                this.deleteRow.next(row[this.gridOptions.rowId]);                
            }
        });
    }

    onClearSearch() {
        this.searchQuery = '';
        this.onSearchData();
    }
     
    
    onGridSort(row: IColumnDefs): void {
        this.sortColumn = row.column;
        this.sortDirection = !this.sortDirection ;
    }
     
    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        this.pager = this.pagingService.paginationProperties(this.gridData.length, page, this.selectPageSize);  
        
        this.pagedItems = this.gridData.slice(this.pager.firstIndex, this.pager.lastIndex + 1);
    }

    saveOnEnter(event: any, row: any,chemId: string): any {
        if (event.keyCode === 13) {
            if (chemId) {
                this.onUpdateRow(row)
            } 
            event.preventDefault();
        } else if (event.keyCode === 27) {
            this.cancel(row)
        }

    }



    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
        if (event.keyCode === 27) {
            this.onResetAddNewForm();
            if (this.searchQuery) {
                this.searchQuery = '';
                this.onSearchData();
            }
            this.pagedItems.forEach((item, i) => {
                item.edit = false;
            });
        }
    }

   



    
}