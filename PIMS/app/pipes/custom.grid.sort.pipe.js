"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CustomGridSortPipe = /** @class */ (function () {
    function CustomGridSortPipe() {
    }
    CustomGridSortPipe.prototype.transform = function (data, fieldProperty, sortingDirection) {
        return data.sort(function (value, item) {
            var a = value[fieldProperty], b = item[fieldProperty];
            if (a === undefined && b === undefined) {
                return 0;
            }
            if (a === undefined && b !== undefined) {
                return sortingDirection ? 1 : -1;
            }
            if (a && b === undefined) {
                return sortingDirection ? -1 : 1;
            }
            if (fieldProperty === 'Date') {
                a = Number(new Date(value[fieldProperty]));
                b = Number(new Date(item[fieldProperty]));
            }
            if (a === b) {
                return 0;
            }
            if (a === null && b !== null) {
                return sortingDirection ? 1 : -1;
            }
            if (a !== null && b === null) {
                return sortingDirection ? 1 : -1;
            }
            return sortingDirection ? (a.toString().toLowerCase() >
                b.toString().toLowerCase() ? -1 : 1) :
                (b.toString().toLowerCase() > a.toString().toLowerCase() ? -1 : 1);
        });
    };
    CustomGridSortPipe = __decorate([
        core_1.Pipe({
            name: 'customGridSort'
        })
    ], CustomGridSortPipe);
    return CustomGridSortPipe;
}());
exports.CustomGridSortPipe = CustomGridSortPipe;
