
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customGridSort'
})
export class CustomGridSortPipe implements PipeTransform {

    transform(data: Array<any>, fieldProperty: string, sortingDirection: boolean): Array<string> {
        return data.sort((value: any, item: any) => {
            let a = value[fieldProperty], b = item[fieldProperty];

        if (a === undefined && b === undefined) {
            return 0;
        }
        if (a === undefined && b !== undefined) {
            return sortingDirection ? 1 : -1;
        }
        if (a && b === undefined) {
            return sortingDirection ? -1 : 1;
        }
        if (fieldProperty === 'Date') {
            a = Number(new Date(value[fieldProperty]));
            b = Number(new Date(item[fieldProperty]));
        }
        if (a === b) {
            return 0;
       }
       if (a === null && b !== null) {
           return sortingDirection ? 1 : -1;
       }
       if (a !== null && b === null) {
           return sortingDirection ? 1 : -1;
       }
        return sortingDirection ? (a.toString().toLowerCase() >
            b.toString().toLowerCase() ? -1 : 1) :
            (b.toString().toLowerCase() > a.toString().toLowerCase() ? -1 : 1);
    });
    
}
}
