﻿import {  Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { IRsmTable } from './../Igrid.interface';

@Injectable()
export class RsmApiService {

    constructor(private http: Http) { }

    //Get all rows
    getRsmData(): Observable<IRsmTable[]> {
        return this.http.get('api/RsmApi').map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    // Add a row
    addNewRow(body: Object): Observable<IRsmTable[]> {
        return this.http.post('api/RsmApi', body)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error)); 
    } 

    // Update a row
    updateRow(body: Object): Observable<IRsmTable[]> {
        return this.http.put('api/RsmApi/'+body['id'], body['item']) 
            .map((res: Response) => res.json()) 
            .catch((error: any) => Observable.throw(error)); 
    }

    // Delete a row
    deleteRow(id: number): Observable<IRsmTable[]> {
        return this.http.delete('api/RsmApi/' + id) 
            .map((res: Response) => res.json()) 
            .catch((error: any) => Observable.throw(error));
    } 

    // Upload a sheet
    uploadSheet(body: Object): Observable<IRsmTable[]> {
        return this.http.post('api/saveExcelData', body)
            .map((res: Response) => res.json()) 
            .catch((error: any) => Observable.throw(error)); 
    } 

    //Get activity log data
    getLogData(): Observable<IRsmTable[]> {
        return this.http.get('api/LoggedData').map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error));
    }

}