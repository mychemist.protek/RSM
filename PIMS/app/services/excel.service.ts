﻿import { Injectable } from '@angular/core';
import  'file-saver';
import * as XLSX from 'ts-xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelService {

    constructor() { }

    public exportAsExcelFile(json: any[], excelFileName: string): void {
        let utils : any = XLSX.utils;
        const worksheet: XLSX.IWorkSheet = utils.json_to_sheet(json);
       // const workbook: XLSX.WorkBook  = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const workbook: any  = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }

}