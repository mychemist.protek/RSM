"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_toastr_1 = require("ng2-toastr/ng2-toastr");
var custom_grid_component_1 = require("./custom-grid/custom.grid.component");
var XLSX = require("ts-xlsx");
var pagination_service_1 = require("./custom-grid/pagination.service");
var excel_service_1 = require("./services/excel.service");
var ngx_progressbar_1 = require("ngx-progressbar");
var rsmApi_service_1 = require("./services/rsmApi.service");
var AppComponent = /** @class */ (function () {
    function AppComponent(excelService, toastr, vcr, ngProgress, pagingService, rsmApiService) {
        this.excelService = excelService;
        this.toastr = toastr;
        this.ngProgress = ngProgress;
        this.pagingService = pagingService;
        this.rsmApiService = rsmApiService;
        this.sortDirection = true;
        this.pager = {};
        this.sortColumn = 'Date';
        this.gridOptions = {
            columnDefs: [
                {
                    column: 'SUPPLIER',
                    columnName: 'Supplier Name',
                    edit: true,
                    type: 'text',
                    cssClass: 'input-supplier'
                },
                {
                    column: 'CODE',
                    columnName: 'Code',
                    edit: true,
                    type: 'text',
                    cssClass: 'input-code'
                },
                {
                    column: 'VCODE',
                    columnName: 'Vcode',
                    edit: true,
                    type: 'text',
                    cssClass: 'input-vcode'
                },
                {
                    column: 'PLU',
                    columnName: 'PLU',
                    edit: true,
                    type: 'number',
                    cssClass: 'input-plu'
                },
                {
                    column: 'MychemId',
                    columnName: 'MyChemID',
                    edit: false,
                    type: 'number',
                    cssClass: 'input-chemId'
                },
                {
                    column: 'DESCRIPTION_',
                    columnName: 'Description',
                    edit: true,
                    type: 'text',
                    cssClass: 'input-desc'
                },
                {
                    column: 'COST',
                    columnName: 'Cost Price',
                    edit: true,
                    type: 'text',
                    cssClass: 'input-cost'
                },
                {
                    column: 'PROMO__',
                    columnName: 'Promotional Price',
                    edit: true,
                    type: 'text',
                    cssClass: 'input-promo'
                },
                {
                    column: 'MOQ',
                    columnName: 'MOQ',
                    edit: true,
                    type: 'number',
                    cssClass: 'input-moq'
                },
                {
                    column: 'CONTACT_DETAILS',
                    columnName: 'Contact Information',
                    edit: true,
                    type: 'email',
                    cssClass: 'input-contact_details'
                },
                {
                    column: 'NOTES',
                    columnName: 'Notes',
                    edit: true,
                    type: 'text',
                    cssClass: 'input-notes'
                }
            ]
        };
        this.showSpinner = true;
        this.percentage = 0;
        this.errorListArray = [];
        this.selectPageSize = 100;
        this.activityLogData = [];
        this.pagedData = [];
        this.toastr.setRootViewContainerRef(vcr);
        this.excelService = excelService;
        this.get();
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent.prototype.onSortHandler = function (column) {
        this.sortColumn = column;
        this.sortDirection = !this.sortDirection;
    };
    AppComponent.prototype.addNewRow = function (row) {
        var _this = this;
        this.rsmApiService.addNewRow(row)
            .subscribe(function (res) {
            _this.customGridComponent.onResetAddNewForm();
            _this.toastr.success('Product added Successfully !');
            _this.get();
        }, function (error) {
            if (error.statusText === 'Conflict') {
                _this.toastr.error('MyChemID already exist !');
            }
        });
    };
    AppComponent.prototype.deleteRow = function (id) {
        var _this = this;
        this.rsmApiService.deleteRow(id)
            .subscribe(function (res) {
            _this.toastr.success('Product deleted Successfully !');
            _this.get();
        });
    };
    AppComponent.prototype.updateRow = function (ev) {
        var _this = this;
        this.rsmApiService.updateRow(ev)
            .subscribe(function (res) {
            _this.toastr.success('Product updated Successfully !');
            _this.get();
        });
    };
    AppComponent.prototype.get = function () {
        var _this = this;
        this.showSpinner = true;
        return this.rsmApiService.getRsmData()
            .subscribe(function (res) {
            _this.getActivityLogData();
            _this.showSpinner = false;
            _this.gridOptions.data = res;
            _this.gridOptions.rowId = 'MychemId';
            if (_this.customGridComponent) {
                _this.customGridComponent.updateGrid();
            }
        });
    };
    AppComponent.prototype.setPage = function (page) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        this.pager = this.pagingService.paginationProperties(this.activityLogData.length, page, this.selectPageSize);
        this.pagedData = this.activityLogData.slice(this.pager.firstIndex, this.pager.lastIndex + 1);
    };
    AppComponent.prototype.onSelectPageSize = function () {
        this.setPage(1);
    };
    AppComponent.prototype.get_header_row = function (sheet, utils) {
        var headers = [];
        var range = utils.decode_range(sheet['!ref']);
        var C, R = range.s.r; /* start in the first row */
        /* walk every column in the range */
        for (C = range.s.c; C <= range.e.c; ++C) {
            var cell = sheet[utils.encode_cell({ c: C, r: R })]; /* find the cell in the first row */
            var hdr = "UNKNOWN " + C; // <-- replace with your desired default
            if (cell && cell.t)
                hdr = utils.format_cell(cell);
            headers.push(hdr);
        }
        return headers;
    };
    AppComponent.prototype.uploadSheet = function (event) {
        var file = event.target.files[0];
        var x = new FileReader();
        var that = this;
        if (file && (file.name.substring(file.name.lastIndexOf('.') + 1) == 'xls') || (file.name.substring(file.name.lastIndexOf('.') + 1) == 'xlsx') || (file.name.substring(file.name.lastIndexOf('.') + 1) == 'csv')) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var data = e.target.result;
                //XLSX from js-xlsx library , which I will add in page view page
                var workbook = XLSX.read(data, { type: 'binary' });
                var sheetNames = workbook.SheetNames;
                var sheet = XLSX.utils;
                var excelData = [];
                var columnNames = [];
                sheetNames.forEach(function (element) {
                    if (workbook.Sheets[element]['!ref']) {
                        columnNames = that.get_header_row(workbook.Sheets[element], sheet);
                        excelData = excelData.concat(sheet.sheet_to_row_object_array(workbook.Sheets[element]));
                    }
                });
                if (excelData.length === 0) {
                    that.toastr.error('Cannot upload sheet with empty data');
                    event.target.value = '';
                    return false;
                }
                var errorMsg = '';
                if (columnNames.indexOf('MYCHEM ID') > -1) {
                    errorMsg = 'Missing';
                    that.sheetType = 'MYCHEM ID';
                    if (columnNames.indexOf('PLU') === -1) {
                        errorMsg += ' PLU and';
                    }
                    if (columnNames.indexOf('Catalogue Price') === -1) {
                        errorMsg += ' Catalogue Price and';
                    }
                    if (columnNames.indexOf('Everyday Cost') === -1) {
                        errorMsg += ' Everyday Cost';
                    }
                }
                else if (columnNames.indexOf('MychemId') > -1) {
                    that.sheetType = 'MychemId';
                    errorMsg = 'Missing';
                    if (columnNames.indexOf('PLU') === -1) {
                        errorMsg += ' PLU and';
                    }
                    if (columnNames.indexOf('COST') === -1) {
                        errorMsg += ' COST and';
                    }
                    if (columnNames.indexOf('PROMO $') === -1) {
                        errorMsg += ' PROMO';
                    }
                }
                else {
                    errorMsg = 'Please upload valid sheet';
                }
                if (errorMsg.indexOf('Missing') > -1) {
                    if (errorMsg === 'Missing') {
                        errorMsg = '';
                    }
                    else {
                        errorMsg = errorMsg.substring(0, errorMsg.length - 4);
                    }
                }
                if (errorMsg) {
                    that.toastr.error(errorMsg);
                    event.target.value = '';
                    return false;
                }
                that.errorListArray.length = 0;
                that.errorListArray = excelData.filter(function (row) {
                    var chemId = row[that.sheetType];
                    return (chemId == 'NULL' || chemId === 0 || !(/^\d+$/.test(chemId))) || (row['PLU'] == 'NULL' || row['PLU'] === 0 || !(/^\d+$/.test(row['PLU'])));
                });
                if (that.errorListArray.length > 0) {
                    event.target.value = '';
                    return false;
                }
                //that.showSpinner = true;
                var tempExcelData = [];
                excelData.forEach(function (entry) {
                    var chemId;
                    if (columnNames.indexOf('MYCHEM ID') > -1) {
                        chemId = 'MYCHEM ID';
                    }
                    else {
                        chemId = 'MychemId';
                    }
                    if (!that.checkForMatch(tempExcelData, chemId, entry[chemId])) {
                        tempExcelData.push(entry);
                    }
                    else {
                        that.errorListArray.push(entry);
                    }
                });
                if (that.errorListArray.length > 0) {
                    event.target.value = '';
                    return false;
                }
                tempExcelData.forEach(function (row, ind) {
                    if (columnNames.indexOf('MYCHEM ID') > -1) {
                        var tempObj = {};
                        tempObj['COST'] = row['Everyday Cost'] ? row['Everyday Cost'].trim().replace('$', '') : '';
                        tempObj['PROMO__'] = row['Catalogue Price'] ? row['Catalogue Price'].trim().replace('$', '') : '';
                        tempObj['CODE'] = '';
                        tempObj['VCODE'] = '';
                        tempObj['SUPPLIER'] = '';
                        tempObj['CONTACT_DETAILS'] = '';
                        tempObj['DESCRIPTION_'] = row['DESCRIPTION'];
                        tempObj['MOQ'] = '';
                        tempObj['NOTES'] = '';
                        tempObj['MychemId'] = row['MYCHEM ID'];
                        tempObj['PLU'] = row['PLU'];
                        tempExcelData.splice(ind, 1, tempObj);
                    }
                    else {
                        row['PROMO__'] = row['PROMO $'];
                        row['CONTACT_DETAILS'] = row['CONTACT DETAILS'];
                        row['DESCRIPTION_'] = row['DESCRIPTION '];
                        delete row['PROMO $'];
                        delete row['DESCRIPTION '];
                        delete row['CONTACT DETAILS'];
                    }
                });
                that.ngProgress.start();
                that.rsmApiService.uploadSheet(tempExcelData)
                    .subscribe(function (res) {
                    that.ngProgress.done();
                    event.target.value = '';
                    that.toastr.success('Sheet Uploaded Successfully !');
                    //that.showSpinner = false;
                    that.get();
                }, function (error) {
                    // that.showSpinner = false;
                    that.toastr.error('Failed to upload file');
                });
            };
            reader.onerror = function (ex) {
            };
            reader.readAsBinaryString(file);
        }
        else {
            that.toastr.error('Please upload valid sheet');
        }
    };
    AppComponent.prototype.exportToExcel = function (data) {
        var exportData = data.map(function (item) {
            return {
                'Supplier Name': item['SUPPLIER'],
                'Code': item['CODE'],
                'Vcode': item['VCODE'],
                'PLU': item.PLU,
                'MyChemID': item['MychemId'],
                'Description': item['DESCRIPTION_'],
                'Cost Price': item['COST'],
                'Promotional Price': item['PROMO__'],
                'MOQ': item.MOQ,
                'Contact Information': item['CONTACT_DETAILS'],
                'Notes': item.NOTES
            };
        });
        this.excelService.exportAsExcelFile(exportData, 'RSM sheet');
    };
    AppComponent.prototype.getActivityLogData = function () {
        var _this = this;
        return this.rsmApiService.getLogData()
            .subscribe(function (res) {
            var logData = res;
            _this.activityLogData = logData.sort(function (a, b) {
                return new Date(b.Date).getTime() - new Date(a.Date).getTime();
            });
            _this.setPage(1);
        });
    };
    AppComponent.prototype.checkForMatch = function (array, propertyToMatch, valueToMatch) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][propertyToMatch] == valueToMatch)
                return true;
        }
        return false;
    };
    AppComponent.prototype.showErrorMsgFn = function (plu, chemId) {
        var msg = 'Invalid ' + (isNaN(plu) || !plu ? 'PLU and ' : '') + (isNaN(chemId) || !chemId ? 'MychemId' : '');
        if (msg === 'Invalid ') {
            return 'MychemId ' + chemId + ' already exist.';
        }
        return msg.indexOf('and') === msg.length - 4 ? msg.slice(0, msg.length - 4) : msg;
    };
    AppComponent.prototype.closeErrorWindow = function () {
        this.errorListArray.length = 0;
    };
    __decorate([
        core_1.ViewChild(custom_grid_component_1.CustomGridComponent),
        __metadata("design:type", custom_grid_component_1.CustomGridComponent)
    ], AppComponent.prototype, "customGridComponent", void 0);
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        }),
        __metadata("design:paramtypes", [excel_service_1.ExcelService, ng2_toastr_1.ToastsManager, core_1.ViewContainerRef, ngx_progressbar_1.NgProgress, pagination_service_1.PagingService, rsmApi_service_1.RsmApiService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
