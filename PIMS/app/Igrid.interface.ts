﻿export interface IRsmTable {
    SUPPLIER?: string,
    CODE?: string,
    VCODE?: string,
    PLU?: number,
    MychemId?: string,
    DESCRIPTION_?: string,
    COST?: number,
    PROMO__?: number,
    MOQ?: number,
    CONTACT_DETAILS?: string,
    NOTES?: string,
    edit?: boolean,
    editDetails?: object
}
export interface IColumnDefs {
    columnName: string,
    column: string,
    edit: boolean,
    type?: string,
    cssClass: string
}

export interface IGridOptions {
    columnDefs?: Array<IColumnDefs>,
    data?: Array<IRsmTable>,
    rowId?: string

}

export interface IPagination {
    firstPage?: number,
    lastPage?: number,
    firstIndex?: number,
    lastIndex?: number,
    totalPageLinkButtons?: number,
    totalItems?: number,
    currentPage?: number,
    pageSize?: number,
    totalPages?: number,
    pages?: Array<any>,
}