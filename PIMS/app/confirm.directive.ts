﻿import { Component, Output, HostListener, EventEmitter, Directive, ElementRef, NgModule, Input } from '@angular/core';
import '/node_modules/jquery-confirm/js/jquery-confirm.js';
declare const jQuery: {
    confirm: Function
};

@Directive({
    selector: '[confirm]'
})
export class ConfirmDirective {
    @Input() confirm: any;
    @Output('confirm-click') click: any = new EventEmitter();
    
    @HostListener('click', ['$event']) clicked(e: any) {
        jQuery.confirm({
            animationSpeed: 100,
            title: "",
            content: "Are you sure you want to delete MyChemId:" + this.confirm+ " ? ",
            buttons: {
                confirm: {
                    btnClass:'confirm-alert-btn',
                    action: () => this.click.emit()
                },
                cancel: {
                    btnClass: 'cancel-alert-btn',
                    action: () => { }
                }
            }
        });
    }

}