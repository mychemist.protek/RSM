"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
require("/node_modules/jquery-confirm/js/jquery-confirm.js");
var EditableDivDirective = /** @class */ (function () {
    function EditableDivDirective(_elRef, _renderer) {
        this._elRef = _elRef;
        this._renderer = _renderer;
        this.escapeEvent = new core_1.EventEmitter();
        // ControlValueAccessor implementation
        // ====================================
        this._onChange = function (_) { }; // call it if your value changed..
        this._onTouched = function () { }; // call it "on blur" ..
    }
    EditableDivDirective_1 = EditableDivDirective;
    EditableDivDirective.prototype.onChange = function () {
        if (this._onChange) {
            this._onChange(this._elRef.nativeElement.innerText);
        }
    };
    EditableDivDirective.prototype.keyup = function (event) {
        this.onChange();
    };
    // will be called if a values comes in via ngModule !
    EditableDivDirective.prototype.writeValue = function (val) {
        if (!val)
            val = '';
        this._renderer.setElementProperty(this._elRef.nativeElement, 'innerText', val);
    };
    EditableDivDirective.prototype.registerOnChange = function (fn) { this._onChange = fn; };
    EditableDivDirective.prototype.registerOnTouched = function (fn) { this._onTouched = fn; };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], EditableDivDirective.prototype, "escapeEvent", void 0);
    __decorate([
        core_1.HostListener('keyup', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], EditableDivDirective.prototype, "keyup", null);
    EditableDivDirective = EditableDivDirective_1 = __decorate([
        core_1.Directive({
            selector: 'td[contentEditable]',
            providers: [
                {
                    provide: forms_1.NG_VALUE_ACCESSOR,
                    useExisting: core_1.forwardRef(function () { return EditableDivDirective_1; }),
                    multi: true
                }
            ]
        }),
        __metadata("design:paramtypes", [core_1.ElementRef, core_1.Renderer])
    ], EditableDivDirective);
    return EditableDivDirective;
    var EditableDivDirective_1;
}());
exports.EditableDivDirective = EditableDivDirective;
