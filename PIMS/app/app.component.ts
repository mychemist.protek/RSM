import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { IGridOptions, IColumnDefs, IRsmTable, IPagination } from './Igrid.interface';
import { CustomGridComponent } from './custom-grid/custom.grid.component';
import * as XLSX from 'ts-xlsx';
import { PagingService } from './custom-grid/pagination.service';
import { ExcelService } from './services/excel.service';
import { NgProgress } from 'ngx-progressbar';
import { RsmApiService } from './services/rsmApi.service';
@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {
    @ViewChild(CustomGridComponent) customGridComponent: CustomGridComponent;
    sortDirection: boolean = true;
    pager: IPagination = {};
    sortColumn: string = 'Date';
    public gridOptions: IGridOptions = {
        columnDefs: [
            {
                column: 'SUPPLIER',
                columnName: 'Supplier Name',
                edit: true,
                type: 'text',
                cssClass: 'input-supplier'
            },
            {
                column: 'CODE',
                columnName: 'Code',
                edit: true,
                type: 'text',
                cssClass: 'input-code'
            },      

            {
                column: 'VCODE',
                columnName: 'Vcode',
                edit: true,
                type: 'text',
                cssClass: 'input-vcode'
            },
            {
                column: 'PLU',
                columnName: 'PLU',
                edit: true,
                type: 'number',
                cssClass: 'input-plu'
            },
            {
                column: 'MychemId',
                columnName: 'MyChemID',
                edit: false,
                type: 'number',
                cssClass: 'input-chemId'
            },
            {
                column: 'DESCRIPTION_',
                columnName: 'Description',
                edit: true,
                type: 'text',
                cssClass: 'input-desc'
            },
            {
                column: 'COST',
                columnName: 'Cost Price',
                edit: true,
                type: 'text',
                cssClass: 'input-cost'
            },

            {
                column: 'PROMO__',
                columnName: 'Promotional Price',
                edit: true,
                type: 'text',
                cssClass: 'input-promo'
            },

            {
                column: 'MOQ',
                columnName: 'MOQ',
                edit: true,
                type: 'number',
                cssClass: 'input-moq'
            },
            {
                column: 'CONTACT_DETAILS',
                columnName: 'Contact Information',
                edit: true,
                type: 'email',
                cssClass: 'input-contact_details'
            },
            {
                column: 'NOTES',
                columnName: 'Notes',
                edit: true,
                type: 'text',
                cssClass: 'input-notes'
            }
            
           
           

        ]
    };
    showSpinner: boolean = true;
    percentage: Number = 0;
    errorListArray: Array<any> = [];
    sheetType: string;

    constructor(private excelService: ExcelService, public toastr: ToastsManager, vcr: ViewContainerRef, public ngProgress: NgProgress, private pagingService: PagingService, public rsmApiService: RsmApiService) {
        this.toastr.setRootViewContainerRef(vcr);
        this.excelService = excelService;
        this.get();
    }

    ngOnInit() {
        
    }


    onSortHandler(column: string) {
        this.sortColumn = column;
        this.sortDirection = !this.sortDirection;
    }

    addNewRow(row: IRsmTable) {
        this.rsmApiService.addNewRow(row)
            .subscribe(res => {
                this.customGridComponent.onResetAddNewForm();
                this.toastr.success('Product added Successfully !');
                this.get();
            }, (error) => {
                if (error.statusText === 'Conflict') {
                    this.toastr.error('MyChemID already exist !');
                }
            }
        )
    }

    deleteRow(id: number) {
        this.rsmApiService.deleteRow(id)
            .subscribe(res => {
                this.toastr.success('Product deleted Successfully !');
                this.get();
            }            
        )
    }

    updateRow(ev: object) {
        this.rsmApiService.updateRow(ev)
            .subscribe(res => {
                this.toastr.success('Product updated Successfully !');
                this.get();
            })
    
    }

    get() {
        this.showSpinner = true;
        return this.rsmApiService.getRsmData()
            .subscribe(res => {
                this.getActivityLogData();
                this.showSpinner = false;
                this.gridOptions.data = res;                
                this.gridOptions.rowId = 'MychemId';
                if (this.customGridComponent) {
                    this.customGridComponent.updateGrid();
                }
            })
    }
    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        this.pager = this.pagingService.paginationProperties(this.activityLogData.length, page, this.selectPageSize);

        this.pagedData = this.activityLogData.slice(this.pager.firstIndex, this.pager.lastIndex + 1);
    }
    selectPageSize: number = 100;
    onSelectPageSize() {
        this.setPage(1);
    }

    get_header_row(sheet:any,utils:any) {
        var headers = [];
        var range = utils.decode_range(sheet['!ref']);
        var C, R = range.s.r; /* start in the first row */
        /* walk every column in the range */
        for (C = range.s.c; C <= range.e.c; ++C) {
            var cell = sheet[utils.encode_cell({ c: C, r: R })] /* find the cell in the first row */

            var hdr = "UNKNOWN " + C; // <-- replace with your desired default
            if (cell && cell.t) hdr = utils.format_cell(cell);

            headers.push(hdr);
        }
        return headers;
    }

    uploadSheet(event: any) {
        let file = event.target.files[0];
        let x = new FileReader();
        let that = this;
        if (file && (file.name.substring(file.name.lastIndexOf('.') + 1) == 'xls') || (file.name.substring(file.name.lastIndexOf('.') + 1) == 'xlsx') || (file.name.substring(file.name.lastIndexOf('.') + 1) == 'csv')) {
            var reader = new FileReader();
            reader.onload = function (e: any) {
                let data = e.target.result;
                //XLSX from js-xlsx library , which I will add in page view page
                let workbook: XLSX.IWorkBook = XLSX.read(data, { type: 'binary' });
                let sheetNames = workbook.SheetNames;
                let sheet: any = XLSX.utils;
                let excelData: Array<any> = [];
                let columnNames: Array<any> = [];
                sheetNames.forEach((element) => {
                    if (workbook.Sheets[element]['!ref']) {
                        columnNames = that.get_header_row(workbook.Sheets[element], sheet);
                        excelData = excelData.concat(sheet.sheet_to_row_object_array(workbook.Sheets[element]));
                    }
                });

                if (excelData.length === 0) {
                    that.toastr.error('Cannot upload sheet with empty data');
                    event.target.value = '';
                    return false;
                }

                let errorMsg: string = '';
                if (columnNames.indexOf('MYCHEM ID') > -1) {
                    errorMsg = 'Missing';
                    that.sheetType = 'MYCHEM ID';
                    if (columnNames.indexOf('PLU') === -1) {
                        errorMsg += ' PLU and';
                    }
                    if (columnNames.indexOf('Catalogue Price') === -1) {
                        errorMsg += ' Catalogue Price and';
                    }
                    if (columnNames.indexOf('Everyday Cost') === -1){
                        errorMsg += ' Everyday Cost';
                    }
                }
                else if (columnNames.indexOf('MychemId') > -1) {
                    that.sheetType = 'MychemId';
                    errorMsg = 'Missing';
                    if (columnNames.indexOf('PLU') === -1) {
                        errorMsg += ' PLU and';
                    }
                    if (columnNames.indexOf('COST') === -1) {
                        errorMsg += ' COST and';
                    }
                    if (columnNames.indexOf('PROMO $') === -1) {
                        errorMsg += ' PROMO';
                    }
                }
                else {
                    errorMsg = 'Please upload valid sheet';
                }

                if (errorMsg.indexOf('Missing') > -1) {
                    if (errorMsg === 'Missing') {
                        errorMsg = '';
                    } else {
                        errorMsg = errorMsg.substring(0, errorMsg.length - 4);
                    }
                }

                if (errorMsg) {
                    that.toastr.error(errorMsg);
                    event.target.value = '';
                    return false;
                }
                that.errorListArray.length = 0;
                that.errorListArray = excelData.filter((row) => {
                    let chemId: any = row[that.sheetType];
                    return (chemId == 'NULL' || chemId === 0 || !(/^\d+$/.test(chemId))) || (row['PLU'] == 'NULL' || row['PLU'] === 0 || !(/^\d+$/.test(row['PLU'])));
                    
                });

                if (that.errorListArray.length > 0) {
                    event.target.value = '';
                    return false;
                }

                //that.showSpinner = true;
                let tempExcelData: Array<any> = [];
                excelData.forEach(function (entry) {
                    let chemId: any;
                    if (columnNames.indexOf('MYCHEM ID')>-1) {
                        chemId = 'MYCHEM ID';
                    } else {
                        chemId = 'MychemId';
                    }

                    if (!that.checkForMatch(tempExcelData, chemId, entry[chemId])) {
                        tempExcelData.push(entry);
                    } else {
                        that.errorListArray.push(entry);
                    }
                });
                if (that.errorListArray.length > 0) {
                    event.target.value = '';
                    return false;
                }

                tempExcelData.forEach((row, ind) => {

                    if (columnNames.indexOf('MYCHEM ID')>-1) {
                        let tempObj = {};
                        tempObj['COST'] = row['Everyday Cost'] ? row['Everyday Cost'].trim().replace('$', ''):'';
                        tempObj['PROMO__'] = row['Catalogue Price'] ? row['Catalogue Price'].trim().replace('$', ''):'';
                        tempObj['CODE'] = '';
                        tempObj['VCODE'] = '';
                        tempObj['SUPPLIER'] = '';
                        tempObj['CONTACT_DETAILS'] = '';
                        tempObj['DESCRIPTION_'] = row['DESCRIPTION'];
                        tempObj['MOQ'] = '';
                        tempObj['NOTES'] = '';
                        tempObj['MychemId'] = row['MYCHEM ID'];
                        tempObj['PLU'] = row['PLU'];
                        tempExcelData.splice(ind, 1, tempObj);
                    } else {
                        row['PROMO__'] = row['PROMO $'];
                        row['CONTACT_DETAILS'] = row['CONTACT DETAILS'];
                        row['DESCRIPTION_'] = row['DESCRIPTION '];
                        delete row['PROMO $'];
                        delete row['DESCRIPTION '];
                        delete row['CONTACT DETAILS'];
                    }

                });
                that.ngProgress.start();
                that.rsmApiService.uploadSheet(tempExcelData)
                    .subscribe(res => {
                        that.ngProgress.done();
                        event.target.value = '';
                        that.toastr.success('Sheet Uploaded Successfully !');
                        //that.showSpinner = false;
                        that.get();
                    },
                    error => {
                       // that.showSpinner = false;
                        that.toastr.error('Failed to upload file');
                    });
            }
            reader.onerror = function (ex) {
            }
            reader.readAsBinaryString(file);
        } else {
            that.toastr.error('Please upload valid sheet');
        }
    }

    exportToExcel(data: any) {    
        let exportData = data.map((item: any) => {
            return {
                'Supplier Name': item['SUPPLIER'],
                'Code': item['CODE'],
                'Vcode': item['VCODE'],
                'PLU': item.PLU,
                'MyChemID': item['MychemId'],
                'Description': item['DESCRIPTION_'],
                'Cost Price': item['COST'],
                'Promotional Price': item['PROMO__'],
                'MOQ': item.MOQ,
                'Contact Information': item['CONTACT_DETAILS'],
                'Notes': item.NOTES
            }
        });
        this.excelService.exportAsExcelFile(exportData, 'RSM sheet');
    }

    activityLogData: any = [];
    pagedData: any = [];
    getActivityLogData() {
        return this.rsmApiService.getLogData()
            .subscribe(res => {
                let logData = res;
                this.activityLogData = logData.sort((a: any, b: any) =>
                    new Date(b.Date).getTime() - new Date(a.Date).getTime()
                );
                this.setPage(1);
            })
    }

    checkForMatch(array:any, propertyToMatch:any, valueToMatch:any): boolean {
        for (var i = 0; i < array.length; i++) {
            if (array[i][propertyToMatch] == valueToMatch)
                return true;
        }
        return false;
    }

    showErrorMsgFn(plu: any, chemId: any): any {
        let msg = 'Invalid ' + (isNaN(plu) || !plu ? 'PLU and ' : '') + (isNaN(chemId) || !chemId ? 'MychemId' : '');
        if (msg === 'Invalid ') {
            return 'MychemId '+chemId + ' already exist.';
        }
        return msg.indexOf('and') === msg.length - 4 ? msg.slice(0, msg.length - 4) : msg;
    }

    closeErrorWindow() {
        this.errorListArray.length = 0;
    }
}
