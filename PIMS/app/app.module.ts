import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { AppComponent } from './app.component';
import { CustomGridComponent } from './custom-grid/custom.grid.component';
import { CustomGridSortPipe } from './pipes/custom.grid.sort.pipe';
import { PagingService } from './custom-grid/pagination.service'
import { ExcelService } from './services/excel.service';
import { ConfirmDirective } from './confirm.directive';
import { EditableDivDirective } from './directives/content.editable.directive';
//import { ProgressHttpModule } from 'angular-progress-http';
import { TAB_COMPONENTS } from './tab-component/Tabset';
import { SortGridPipe } from './pipes/sortgrid.pipe';
import { NgProgressModule } from 'ngx-progressbar';
import { RsmApiService } from './services/rsmApi.service';
@NgModule({ 
    imports: [BrowserModule, HttpModule, FormsModule, BrowserAnimationsModule, ToastModule.forRoot(), NgProgressModule ],
    declarations: [AppComponent,
        CustomGridComponent, CustomGridSortPipe, ConfirmDirective, EditableDivDirective, TAB_COMPONENTS, SortGridPipe],
    providers: [CustomGridSortPipe, PagingService, ExcelService, RsmApiService],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
