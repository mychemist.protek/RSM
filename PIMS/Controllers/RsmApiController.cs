﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
//using EntityFramework.Extensions;
using PIMS.Models;

namespace PIMS.Controllers
{
    public class RsmApiController : ApiController
    {
        public RsmApiController() {
        }
        private RSMEntities db = new RSMEntities();

        // GET: api/RsmApi
        public IQueryable<ReplenishmentSupplierMasterTable> GetReplenishmentSupplierMasterTables()
        {
           return db.ReplenishmentSupplierMasterTables;
        }

        [HttpPost]
        [Route("api/saveExcelData")]
        public IHttpActionResult saveExcelData(List<ReplenishmentSupplierMasterTable> replenishmentSupplierMasterTable)
        {
            int inserted = 0;
            int updated = 0;
            foreach (var i in replenishmentSupplierMasterTable)
            {

                var c = db.ReplenishmentSupplierMasterTables.Where(a => a.MychemId.Equals(i.MychemId)).FirstOrDefault();
                if (c != null)
                {
                    updated++;
                    c.PROMO__ = i.PROMO__;
                    c.COST = i.COST;
                    db.Entry(c).State = EntityState.Modified;
                }
                else
                {
                    inserted++;
                    db.ReplenishmentSupplierMasterTables.Add(i);
                }
                db.SaveChanges();
            }
            return Ok(db.ReplenishmentSupplierMasterTables);
        }

        // GET: api/RsmApi/5
        [ResponseType(typeof(ReplenishmentSupplierMasterTable))]
        public IHttpActionResult GetReplenishmentSupplierMasterTable(double id)
        {
            ReplenishmentSupplierMasterTable replenishmentSupplierMasterTable = db.ReplenishmentSupplierMasterTables.Find(id);
            if (replenishmentSupplierMasterTable == null)
            {
                return NotFound();
            }

            return Ok(replenishmentSupplierMasterTable);
        }

        // PUT: api/RsmApi/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutReplenishmentSupplierMasterTable(double id, ReplenishmentSupplierMasterTable replenishmentSupplierMasterTable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != replenishmentSupplierMasterTable.MychemId)
            {
                return BadRequest();
            }

            db.Entry(replenishmentSupplierMasterTable).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReplenishmentSupplierMasterTableExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RsmApi
        [ResponseType(typeof(ReplenishmentSupplierMasterTable))]
        public IHttpActionResult PostReplenishmentSupplierMasterTable(ReplenishmentSupplierMasterTable replenishmentSupplierMasterTable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ReplenishmentSupplierMasterTables.Add(replenishmentSupplierMasterTable);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ReplenishmentSupplierMasterTableExists(replenishmentSupplierMasterTable.MychemId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = replenishmentSupplierMasterTable.MychemId }, replenishmentSupplierMasterTable);
        }

        // DELETE: api/RsmApi/5
        [ResponseType(typeof(ReplenishmentSupplierMasterTable))]
        public IHttpActionResult DeleteReplenishmentSupplierMasterTable(double id)
        {
            ReplenishmentSupplierMasterTable replenishmentSupplierMasterTable = db.ReplenishmentSupplierMasterTables.Find(id);
            if (replenishmentSupplierMasterTable == null)
            {
                return NotFound();
            }

            db.ReplenishmentSupplierMasterTables.Remove(replenishmentSupplierMasterTable);
            db.SaveChanges();

            return Ok(replenishmentSupplierMasterTable);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReplenishmentSupplierMasterTableExists(double id)
        {
            return db.ReplenishmentSupplierMasterTables.Count(e => e.MychemId == id) > 0;
        }
    }
}