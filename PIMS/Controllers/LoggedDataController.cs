﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PIMS.Models;

namespace PIMS.Controllers
{
    public class LoggedDataController : ApiController
    {
        private RSMEntities db = new RSMEntities();

        // GET: api/LoggedData
        public IQueryable<LoggedData> GetLoggedDatas()
        {
            return db.LoggedDatas;
        }

        // GET: api/LoggedData/5
        [ResponseType(typeof(LoggedData))]
        public IHttpActionResult GetLoggedData(int id)
        {
            LoggedData loggedData = db.LoggedDatas.Find(id);
            if (loggedData == null)
            {
                return NotFound();
            }

            return Ok(loggedData);
        }

        // PUT: api/LoggedData/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLoggedData(int id, LoggedData loggedData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != loggedData.id)
            {
                return BadRequest();
            }

            db.Entry(loggedData).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LoggedDataExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LoggedData
        [ResponseType(typeof(LoggedData))]
        public IHttpActionResult PostLoggedData(LoggedData loggedData)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LoggedDatas.Add(loggedData);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = loggedData.id }, loggedData);
        }

        // DELETE: api/LoggedData/5
        [ResponseType(typeof(LoggedData))]
        public IHttpActionResult DeleteLoggedData(int id)
        {
            LoggedData loggedData = db.LoggedDatas.Find(id);
            if (loggedData == null)
            {
                return NotFound();
            }

            db.LoggedDatas.Remove(loggedData);
            db.SaveChanges();

            return Ok(loggedData);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LoggedDataExists(int id)
        {
            return db.LoggedDatas.Count(e => e.id == id) > 0;
        }
    }
}